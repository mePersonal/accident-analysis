window.colors = ["#FF0000",
    "#00FF00",
    "#0000FF",
    "#ff00d0",
    "#c8ff00",
    "#00f1ff",
    "#ff8600",
    "#bcff76",
    "#ff7956"
];

window.icons = {
    car: 'icons/car.png',
    car_inj: 'icons/car-inj.png',
    car_death: 'icons/car-death.png',
    man: 'icons/man.png',
    man_inj: 'icons/man-inj.png',
    man_death: 'icons/man-death.png'
};

function array_unique(arr) {
    var tmp_arr = new Array();
    for (i = 0; i < arr.length; i++) {
        if (tmp_arr.indexOf(arr[i]) == "-1") {
            tmp_arr.push(arr[i]);
        }
    }
    return tmp_arr;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

//Добавление рэндомного смещения к точке на 2-3 метра в стороны, иначе все точки по Яндексу на одном адресе будут наслаиваться
function addOffset(coords) {
    coords.lat = coords.lat + getRandomArbitrary(-200,200)/1000000;
    coords.lng = coords.lng + getRandomArbitrary(-200,200)/1000000;

    return coords;
}

//Показать диалог инструмента "Координаты", который позволяет показать координаты произвольной точки на карте
function showPointCoordinateTool () {
    $('#pointCoordinates').dialog('open');
}

//Ставим точку для инструмента "Координаты"
function setPoint (latlng) {
    clearPoint();
    window.point = L.circleMarker(latlng, {
        radius: 7,
        opacity: 1,
        fill: true,
        fillColor: '#00ff2c',
        color: '#00ff2c',
        fillOpacity: 1
    }).addTo(this.map);
}
//Очищаем точку с координатами
function clearPoint () {
    if (window.point!=undefined) {
        window.map.removeLayer(window.point);
        delete window.point;
    }
}

//Обрабатываем загруженный файл
function receivedText() {
    delete window.dtpList;
    window.dtpList = [];
    window.guiltyCarList = [];
    window.guiltyPedList = [];

    window.minDate = new Date();
    window.maxDate = new Date('1900/01/01');

    if (window.fileName.indexOf('.json')>0) {
        window.dtpList = JSON.parse(window.fr.result);
        window.dtpList.forEach(function(accident){
            let arrDate = accident.date.split('.');
            accident.date = new Date(arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0]);
            let tmpDate = accident.date;

            accident.inj = parseInt(accident.inj);
            accident.death = parseInt(accident.death);

            accident.latLng.lat = parseFloat(accident.latLng.lat);
            accident.latLng.lng = parseFloat(accident.latLng.lng);

            if (accident.latLng_y!=undefined) {
                accident.latLng_y.lat = parseFloat(accident.latLng_y.lat);
                accident.latLng_y.lng = parseFloat(accident.latLng_y.lng);
            }

            if (tmpDate < window.minDate) window.minDate = tmpDate;
            if (tmpDate > window.maxDate) window.maxDate = tmpDate;

            let addrArr = [];
            if (accident.addr_y!=undefined) addrArr = accident.addr_y.split(',');

            //Принимаем координаты Яндекса только если адрес по Яндекс.Геокодированию полный (а не просто определился город)
            if (accident.latLng_y!=undefined&&addrArr.length>2) {
                let pointOrigin = L.latLng(accident.latLng.lat, accident.latLng.lng);
                let pointYandex = L.latLng(accident.latLng_y.lat, accident.latLng_y.lng);

                accident.distance = pointOrigin.distanceTo(pointYandex);
            } else {
                accident.distance = 0;
            }

            if (accident.carGuilty != undefined) {
                let tmpArr = accident.carGuilty.split(', ');
                let tmpArr2 = [];
                let strGuilty = '';
                tmpArr.forEach(function(element) {
                    let firstLetter = element.substr(0,1);
                    if (firstLetter == firstLetter.toUpperCase() && strGuilty != '') {
                        //Уже что-то было, следующее начинается с большой буквы, значит добавляем предыдущее в массив
                        tmpArr2.push(strGuilty);
                        strGuilty = element;
                    } else if (strGuilty!= '') {
                        strGuilty += ', '+element;
                    } else {
                        if (element!='')
                            strGuilty = element;
                    }
                });
                tmpArr2.push(strGuilty);
                tmpArr2.forEach(function (element) {
                    if (element!="")
                        window.guiltyCarList.push(element);
                })
            }

            if (accident.pedGuilty != undefined) {
                let tmpArr = accident.pedGuilty.split(', ');
                let tmpArr2 = [];
                let strGuilty = '';
                tmpArr.forEach(function(element) {
                    let firstLetter = element.substr(0,1);
                    if (firstLetter == firstLetter.toUpperCase() && strGuilty != '') {
                        //Уже что-то было, следующее начинается с большой буквы, значит добавляем предыдущее в массив
                        tmpArr2.push(strGuilty);
                        strGuilty = element;
                    } else if (strGuilty!= '') {
                        strGuilty += ', '+element;
                    } else {
                        if (element!='')
                            strGuilty = element;
                    }
                });
                tmpArr2.push(strGuilty);
                tmpArr2.forEach(function (element) {
                    if (element!="")
                        window.guiltyPedList.push(element);
                })
            }
        });

        $('#guiltyCarFilter').html('<option value="-1">Без фильтра</option>');
        array_unique(window.guiltyCarList).forEach(function (element) {
            $('#guiltyCarFilter').append('<option value="'+element+'">'+element+'</option>');
        });

        $('#guiltyPedFilter').html('<option value="-1">Без фильтра</option>');
        array_unique(window.guiltyPedList).forEach(function (element) {
            $('#guiltyPedFilter').append('<option value="'+element+'">'+element+'</option>');
        });

        //console.log(array_unique(window.guiltyList));
        //console.log(window.dtpList);

    } else if (window.fileName.indexOf('.xml')>0) { // Загрузка сырого XML от ГИБДД
        let xmlObject = $.parseXML(window.fr.result);
        $(xmlObject).find('tab').each(function () {
            let accident = {};
            accident.type = $(this).find('DTPV').text();
            let arrDate = $(this).find('date').text().split('.');
            accident.date = new Date(arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0]);
            accident.latLng = {lat: $(this).find('COORD_W').text(), lng: $(this).find('COORD_L').text()};
            accident.death = parseInt($(this).find('POG').text());
            accident.inj = parseInt($(this).find('RAN').text());
            accident.addr = $(this).find('street').text() + ', ' + $(this).find('house').text();
            accident.id = $(this).find('kartId').text();

            let tmpDate = accident.date;

            if (tmpDate < window.minDate) window.minDate = tmpDate;
            if (tmpDate > window.maxDate) window.maxDate = tmpDate;
            window.dtpList.push(accident);
        });
    }

    $('#showHidePoints, #showAnalysedPoints').removeAttr('disabled');
}
//Очищаем всё ранее показанное
function clearMap () {
    if (window.tmpSecondPoint!=undefined) {
        window.map.removeLayer(window.tmpSecondPoint);
    }
    if (window.secondPointLine!=undefined) {
        window.map.removeLayer(window.secondPointLine);
    }
    if (window.dtpMapList!=undefined) {
        window.map.removeLayer(window.dtpMapList);
    }
    if (window.dtpCarInjCluster!=undefined) {
        window.map.removeLayer(window.dtpCarInjCluster);
    }
    if (window.dtpPedInjCluster!=undefined) {
        window.map.removeLayer(window.dtpPedInjCluster);
    };
    if (window.heatmapLayer!=undefined) {
        window.map.removeLayer(window.heatmapLayer);
    };
    if (window.pointsArray!=undefined)
        delete(window.pointsArray);
}

//Функция показа второй точки, если она была перенесена или расходится больше порога
function showSecondPoint(accident_id,type) {
    if (window.tmpSecondPoint!=undefined) {
        window.map.removeLayer(window.tmpSecondPoint);
    }
    if (window.secondPointLine!=undefined) {
        window.map.removeLayer(window.secondPointLine);
    }
    window.dtpList.forEach(function (accident) {
        if (accident.id == accident_id) {
            let setCoords,curCoords;
            if (type == 1) { // Показана точка по координатам яндекса. Нужно показать оригинальную координату
                setCoords = accident.latLng;
                curCoords = accident.latLng_y;
            } else {
                setCoords = addOffset(accident.latLng_y);
                curCoords = accident.latLng;
            }

            window.tmpSecondPoint = L.circleMarker(setCoords, {
                radius: 15,
                opacity: 1,
                fill: true,
                fillColor: '#25ff00',
                color: '#00ff2c',
                fillOpacity: 1
            });

            let popup = '<h4 style="margin-bottom:2px;">' + accident.type + '</h4>' +
                '<div style="margin-bottom: 5px">№ ' + accident.id + '</div>' +
                '<div style="margin-bottom: 5px">Коорд. ' + accident.latLng.lat + ', '+accident.latLng.lng+'</div>' +
                '<div>' + accident.addr + '</div>' +
                '<br/><div>' + accident.date.toLocaleDateString("ru-RU") + '</div><div>Пог: ' + accident.death +
                '</div><div>Постр: ' + accident.inj + '</div>' +
                '<div><input type="button" value="Yandex" onclick="showYandexPoint('+accident.id+')"/> </div>';

            window.tmpSecondPoint.bindPopup(popup);

            window.map.addLayer(window.tmpSecondPoint);

            let lineArr = [L.latLng(setCoords),L.latLng(curCoords)];

            window.secondPointLine = L.polyline(lineArr, {color: '#25ff00'}).addTo(map);

            window.map.fitBounds(window.secondPointLine.getBounds());
        }
    });
}

//Отображаем точки по условиям
function showPoints(inputList) {
    clearMap();

    window.pointsArray = [];

    //if ($('#showHidePoints').hasClass('btn-success')) return;

    //window.dtpMapList = L.markerClusterGroup({ chunkedLoading: true });
    window.dtpMapList = L.layerGroup();
    window.dtpCarInjCluster = L.markerClusterGroup({ chunkedLoading: true });
    window.dtpPedInjCluster = L.markerClusterGroup({ chunkedLoading: true });

    /*Заготовки маркеров*/
    let LeafIcon = L.Icon.extend({
        options: {
            iconSize:     [20, 20],
            iconAnchor:   [10, 10],
            popupAnchor:  [10, -20]
        }
    });

    let dtpStat = {novictim:0,inj:0,death:0,dtpInjCnt:0,dtpDeathCnt:0};
    let pedStat = {novictim:0,inj:0,death:0,dtpInjCnt:0,dtpDeathCnt:0};
    let otStat = {novictim:0,inj:0,death:0,dtpInjCnt:0,dtpDeathCnt:0};

    let warnBorder = parseInt($('#border-distance-warn').val());
    let moveBorder = parseInt($('#border-distance-move').val());

    let arrDate, dateFrom, dateTo;

    if ($('#filter-date-from').val()!=''&&$('#filter-date-to').val()!='') {
        arrDate = $('#filter-date-from').val().split('.');
        dateFrom = new Date(arrDate[2]+'/'+arrDate[1]+'/'+arrDate[0]);
        arrDate = $('#filter-date-to').val().split('.');
        dateTo = new Date(arrDate[2]+'/'+arrDate[1]+'/'+arrDate[0]);
    }

    //Обходим все ДТП в списке
    window.dtpList.forEach(function(accident) {
        if (inputList!=undefined) {
            if (jQuery.inArray(accident.id,inputList)== -1) return;
        };

        //1. Определяем тип (с пешеходом или нет)
        let accIcon;
        let flIgnore = false;

        let flCarCluster = false;
        let flPedCluster = false;
        let flOtCluster = false;

        let setCoords = {};

        let addButtonToBaloon = '';

        // Проверяем даты
        let accDate = accident.date;

        if (accDate < dateFrom || accDate > dateTo)
            flIgnore = true;

        //Проверим на фильтр по нарушениям
        let guiltyFilter = $('#guiltyCarFilter').val();
        if (guiltyFilter!='-1') {
            if (accident.carGuilty.indexOf(guiltyFilter) === -1)
                flIgnore = true;
        }

        guiltyFilter = $('#guiltyPedFilter').val();
        if (guiltyFilter!='-1') {
            if (accident.pedGuilty.indexOf(guiltyFilter) === -1)
                flIgnore = true;
        }

        if (parseInt(accident.isOt) == 1) {
            //console.log('OT',accident);
            if (!$('#ot').is(':checked')) flIgnore = true;
            if (accident.death>0) {
                if (!$('#ot-death').is(':checked')) flIgnore = true;

                if (!flIgnore) {
                    otStat.death += accident.death;
                    otStat.dtpDeathCnt++;
                }

                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-death-y.png'});
                    setCoords = addOffset(accident.latLng_y);

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-death-warn.png'});
                    setCoords = accident.latLng;
                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-death.png'});
                    setCoords = accident.latLng;
                }
            } else if (accident.inj>0) {
                if (!$('#ot-injured').is(':checked')) flIgnore = true;
                if (!flIgnore) {
                    otStat.inj += accident.inj;
                    otStat.dtpInjCnt++;
                }

                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-inj-y.png'});
                    setCoords = addOffset(accident.latLng_y);

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-inj-warn.png'});
                    setCoords = accident.latLng;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/ot-inj.png'});
                    setCoords = accident.latLng;
                }
            } else {
                accIcon = new LeafIcon({iconUrl: 'icons/ot.png'});
            }
        } else if (accident.type.toLowerCase().indexOf('пешех')>=0) {
            if (!$('#ped').is(':checked')) flIgnore = true;
            if (accident.death>0) {
                if (!$('#ped-death').is(':checked')) flIgnore = true;
                if (!flIgnore) {
                    pedStat.death += accident.death;
                    pedStat.dtpDeathCnt++;
                }

                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-death-y.png'});
                    setCoords = addOffset(accident.latLng_y);

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-death-warn.png'});
                    setCoords = accident.latLng;
                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-death.png'});
                    setCoords = accident.latLng;
                }
            } else if (accident.inj>0) {
                flPedCluster = true;
                if (!$('#ped-injured').is(':checked')) flIgnore = true;
                if (!flIgnore) {
                    pedStat.inj += accident.inj;
                    pedStat.dtpInjCnt++;
                }

                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-inj-y.png'});
                    setCoords = addOffset(accident.latLng_y);

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-inj-warn.png'});
                    setCoords = accident.latLng;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/man-inj.png'});
                    setCoords = accident.latLng;
                }
            } else {
                accIcon = new LeafIcon({iconUrl: 'icons/man.png'});
            }
        } else { //Машины
            if (!$('#dtp').is(':checked')) flIgnore = true;
            if (accident.death>0) {
                if (!$('#car-death').is(':checked')) flIgnore = true;
                if (!flIgnore) {
                    dtpStat.death += accident.death;
                    dtpStat.dtpDeathCnt++;
                }
                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-death-y.png'});
                    setCoords = accident.latLng_y;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-death-warn.png'});
                    setCoords = accident.latLng;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-death.png'});
                    setCoords = accident.latLng;
                }
            } else if (accident.inj>0) {
                flCarCluster = true;
                if (!$('#car-injured').is(':checked')) flIgnore = true;
                if (!flIgnore) {
                    dtpStat.inj += accident.inj;
                    dtpStat.dtpInjCnt++;
                }
                //Смотрим порог и показываем соответствующие иконки
                if (accident.distance!=undefined&&accident.distance>moveBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-inj-y.png'});
                    setCoords = accident.latLng_y;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',1)"/> </div>';
                } else if (accident.distance!=undefined&&accident.distance>warnBorder) {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-inj-warn.png'});
                    setCoords = accident.latLng;

                    addButtonToBaloon = '<div><input type="button" value="Анализ" onclick="showSecondPoint('+accident.id+',2)"/> </div>';
                } else {
                    accIcon = new LeafIcon({iconUrl: 'icons/car-inj.png'});
                    setCoords = accident.latLng;
                }
            } else {
                accIcon = new LeafIcon({iconUrl: 'icons/car.png'});
            }
        }

        if (!flIgnore) {
            let popup = '<h4 style="margin-bottom:2px;">' + accident.type + '</h4>' +
                '<div style="margin-bottom: 5px">№ ' + accident.id + '</div>' +
                '<div style="margin-bottom: 5px">Коорд. ' + accident.latLng.lat + ', '+accident.latLng.lng+'</div>' +
                (accident.latLng_y!=undefined?'<div style="margin-bottom: 5px">Коорд Yandex. ' + accident.latLng_y.lat + ', '+accident.latLng_y.lng+'</div>':'') +
                '<div style="margin-bottom: 5px">Distance:  ' + accident.distance+'</div>' +
                '<div>' + accident.addr + '</div>' +
                '<div>Yandex: ' + accident.addr_y + '</div>' +
                '<br/><div>' + accident.date.toLocaleDateString("ru-RU") + ' '+accident.time+'</div>' +
                '<div>Участие ОТ: '+(parseInt(accident.isOt) == 1?'Да':'Нет')+'</div>'+
                '<div>Пог: ' + accident.death +
                '</div><div>Постр: ' + accident.inj + '</div>'+addButtonToBaloon+
                '<div>Вина авт.: ' + accident.carGuilty + '</div>'+
                '<div>Вина пешех.: ' + accident.pedGuilty + '</div>';

            if ($('#showPointsAnalysis').hasClass('active')) {
                window.pointsArray.push([setCoords.lat,setCoords.lng]);
                let marker = L.marker(setCoords, {icon: accIcon});
                marker.bindPopup(popup);

                if (flCarCluster && $('#clustCarInj').is(':checked'))
                    window.dtpCarInjCluster.addLayer(marker);
                else if (flPedCluster && $('#clustPedInj').is(':checked'))
                    window.dtpPedInjCluster.addLayer(marker);
                else
                    window.dtpMapList.addLayer(marker);
            } else if ($('#showHeatMapAnalysis').hasClass('active')) {
                if (setCoords.lat!=NaN&&setCoords.lng!=NaN) {
                    //console.log(setCoords);
                    //let tmpLatLngPoint = new L.LatLng(setCoords.lat,setCoords.lng);
                    window.pointsArray.push([setCoords.lat,setCoords.lng,accident.inj+2*accident.death]);
                    //window.pointsArray.push({'lat':setCoords.lat,'lng':setCoords.lng,'count':1});
                }
            }
        }
    });

    if ($('#showPointsAnalysis').hasClass('active')) {
        window.map.addLayer(window.dtpMapList);
        window.map.addLayer(window.dtpCarInjCluster);
        window.map.addLayer(window.dtpPedInjCluster);
    } else if ($('#showHeatMapAnalysis').hasClass('active')) {
        if (window.heatRadiusValue==undefined) window.heatRadiusValue = 1;


        window.heatmapLayer = L.heatLayer(
            window.pointsArray,  {max:window.heatRadiusValue}).addTo(window.map);
        //window.map.addLayer(window.heatmapLayer);

        //console.log(window.heatRadiusValue);

        /*let cfg = {
            // radius should be small ONLY if scaleRadius is true (or small radius is intended)
            // if scaleRadius is false it will be the constant radius used in pixels
            "radius": window.heatRadiusValue,
            "maxOpacity": .6,
            // scales the radius based on map zoom
            "scaleRadius": true,
            // if set to false the heatmap uses the global maximum for colorization
            // if activated: uses the data maximum within the current map boundaries
            //   (there will always be a red spot with useLocalExtremas true)
            "useLocalExtrema": false,
            // which field name in your data represents the latitude - default "lat"
            latField: 'lat',
            // which field name in your data represents the longitude - default "lng"
            lngField: 'lng',
            // which field name in your data represents the data value - default "value"
            valueField: 'count'
        };

        window.heatmapLayer =  new HeatmapOverlay(cfg);
        //console.log(window.heatmapLayer);
        window.map.addLayer(window.heatmapLayer);

        let heatData = {
            max: 8,
            data: window.pointsArray
        };
        window.heatmapLayer.setData(heatData);*/
    }

    //Визуализируем статистику
    $('#carInjured').text(dtpStat.inj + ' (ДТП: '+dtpStat.dtpInjCnt+')');
    $('#carDeath').text(dtpStat.death + ' (ДТП: '+dtpStat.dtpDeathCnt+')');

    $('#pedInjured').text(pedStat.inj + ' (ДТП: '+pedStat.dtpInjCnt+')');
    $('#pedDeath').text(pedStat.death + ' (ДТП: '+pedStat.dtpDeathCnt+')');

    $('#otInjured').text(otStat.inj + ' (ДТП: '+otStat.dtpInjCnt+')');
    $('#otDeath').text(otStat.death + ' (ДТП: '+otStat.dtpDeathCnt+')');

    $('#date-from').text(window.minDate.toLocaleDateString("ru-RU"));
    $('#date-to').text(window.maxDate.toLocaleDateString("ru-RU"));

    if (!window.flFirstBordersSet) {
        window.flFirstBordersSet = true;
        $('#filter-date-from').val(window.minDate.toLocaleDateString("ru-RU"));
        $('#filter-date-to').val(window.maxDate.toLocaleDateString("ru-RU"));
    }

    //Масштабируем карту на точки только если это первичная загрузка после загрузки файла. Иначе пользователь может уже
    //выставить нужное место, а мы его собъём
    if (!window.isFit) {
        window.isFit = true;
        //let tmpLatLngBounds = L.LatLngBounds(window.pointsArray);
        window.map.fitBounds(window.pointsArray);
    }
}

$(document).ready(function(){
    $('#map').height($(window).height());

    window.map = L.map('map', {
        fullscreenControl: true,
        keyboard: false
    }).setView([56.03656842068916, 92.96184539794922], 3);
    this.map = window.map;

    // инициализиурем варианты карты
    let osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    let yndx_sh = new L.Yandex('map');
    let yndx_sat = new L.Yandex('satellite');

    window.map.addLayer(osm);

    let baseMaps = {
        "OpenStreetMap": osm,
        "Yandex (спутник)": yndx_sat,
        "Yandex (схема)": yndx_sh
    };

    this.mapControl = L.control.layers(baseMaps, {},{position: 'topright'});
    this.map.addControl(this.mapControl);

    //добавляем линейку на карту
    let d = new L.Control.Distance({popups: false});
    window.map.addControl(d);

    $('#pointCoordinates').dialog({
        autoOpen: false,
        modal: false,
        resizable: true,
        width:500,
        height:300,
        beforeClose: function(event) {
            if (event.keyCode === $.ui.keyCode.ESCAPE) {
                // ...
                return false;
            }
            window.clearPoint();
        },
        closeOnEscape: false
    });

    $('#legend').dialog({
        autoOpen: true,
        modal: false,
        resizable: true,
        width:500,
        height:930,
        beforeClose: function(event) {
            //return false;
        },
        position: { my: "left top", at: "left bottom", of: window },
        closeOnEscape: false
    });

    $('#legend input[type=checkbox]').on('change',function(){
        showPoints();
    });

    window.flFirstBordersSet = false;
    $('#filter-date-from,#filter-date-to').datepicker({
        onSelect: function(dateText) {
            showPoints();
        }
    });

    $('#setBorderValues').on('click',function () {
        showPoints();
    });

    $('#showHidePoints').on('click',function(){
        if ($(this).is(':disabled')) return;

        if ($(this).hasClass('btn-success')) {
            $(this).removeClass('btn-success').addClass('btn-warning').val('Скрыть');
            showPoints();
        } else {
            $(this).removeClass('btn-warning').addClass('btn-success').val('Показать');
            clearMap();
        }
    });

    this.map.on('click',(e)=>{
        //Обработчик клика по карте.
        if ($('#pointCoordinates').is(':visible')) {
            //Ставим точку куда кликнули
            setPoint(e.latlng);

            $('#mapPointCoordinateLatitude').val(e.latlng.lat);
            $('#mapPointCoordinateLongitude').val(e.latlng.lng);

            let link = 'https://www.google.com/maps?q='+e.latlng.lat+','+e.latlng.lng;
            $('#mapPointCoordinateLink').attr('href',link);
        };
    });

    $('#inputXml').on('change',function(){
        window.isFit = false;
        file = document.getElementById('inputXml').files[0];
        window.fileName = file.name;
        window.fr = new FileReader();
        window.fr.onload = receivedText;
        //fr.readAsText(file);
        window.fr.readAsText(file);
    });

    $('#guiltyCarFilter, #guiltyPedFilter').on('change',function () {
        showPoints();
    })

});