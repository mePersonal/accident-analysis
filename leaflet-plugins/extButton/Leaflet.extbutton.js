L.Control.ExtButton = L.Control.extend({
    options: {
        position: 'topleft',
        title: 'Перейти к точке',
        type: ''
    },

    initialize: function (options) {
        this._button = {};
        this.setButton(options);
    },

    onAdd: function (map) {
        var container = L.DomUtil.create('div', 'leaflet-control-extbutton'+this.options.type+' leaflet-bar leaflet-control');

        this.link = L.DomUtil.create('a', 'leaflet-control-extbutton-button'+this.options.type+' leaflet-bar-part', container);
        this.link.href = '#';
        this.link.title = this.options.title;

        this._map = map;

        L.DomEvent.on(this.link, 'click', L.DomEvent.stop, this);
        L.DomEvent.on(this.link, 'click', this._click, this);
        return container;
    },

    setButton: function (options) {
        var button = {
            'onClick': options.onClick //callback function
        };

        if (options.type!=undefined)
            this.options.type = options.type;

        this._button = button;
    },

    _click: function (e) {
        if(this._button.onClick !== 'undefined') {
            this._button.onClick();
        }
        return false;
    }
});