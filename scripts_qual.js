//Анализ очагов аварийности
$(document).ready(function(){
    $('#analysis').dialog({
        autoOpen: true,
        modal: false,
        resizable: true,
        width:500,
        height:280,
        beforeClose: function(event) {
        },
        position: { my: "left top", at: "right bottom", of: window },
        closeOnEscape: false
    });

/*    $('#heatRadius').slider().on('slide slideStop', function(slideEvt) {
        window.heatRadiusValue = slideEvt.value;
        window.heatmapLayer.setOptions({max:window.heatRadiusValue})
    });*/

    $('#heatRadius').bootstrapSlider().on('slide slideStop', function(slideEvt) {
        window.heatRadiusValue = slideEvt.value;
        window.heatmapLayer.setOptions({max:window.heatRadiusValue})
    });

    /*window.heatSlider.on("slide", function(sliderValue) {
        window.heatRadiusValue = sliderValue;
        console.log(sliderValue);
        window.heatmapLayer.setOptions({max:sliderValue})
        //console.log(sliderValue);
    });*/

    $('#showHeatMapAnalysis').on('click',function(){
        $('#showPointsAnalysis').removeClass('active');
        $('#showHeatMapAnalysis').addClass('active');

        $('#pointsAnalysis').hide();
        $('#heatAnalysis').show();
        showPoints();
        return false;
    });

    $('#showPointsAnalysis').on('click',function(){
        $('#showHeatMapAnalysis').removeClass('active');
        $('#showPointsAnalysis').addClass('active');

        $('#pointsAnalysis').show();
        $('#heatAnalysis').hide();
        showPoints();
        return false;
    });


    $('#showAnalysedPoints').on('click',function(){
        //Здесь нужно составить сеты по расстоянию между дтп, чтобы потом понять выводить или нет
        if (window.dtpSets!=undefined) delete window.dtpSets;

        dtpSets = [];

        let accToShow = [];

        let moveBorder = parseInt($('#border-distance-move').val());
        let radius = parseInt($('#border-analysis-radius').val());
        let maxAccidents = parseInt($('#border-analysis-cntEvents').val());

        //Обходим весь список дтп и сравниваем все дтп друг с другом
        window.dtpList.forEach(function (accident) {
            let accDate = accident.date;

            if ($('#filter-date-from').val()!=''&&$('#filter-date-to').val()!='') {
                let arrDate = $('#filter-date-from').val().split('.');
                let dateFrom = new Date(arrDate[2]+'/'+arrDate[1]+'/'+arrDate[0]);
                arrDate = $('#filter-date-to').val().split('.');
                let dateTo = new Date(arrDate[2]+'/'+arrDate[1]+'/'+arrDate[0]);

                if (accDate < dateFrom || accDate > dateTo)
                    return;
            }

            let setCoords1 = {};

            if (accident.distance!=undefined&&accident.distance>moveBorder) {
                setCoords1 = accident.latLng_y;
            } else {
                setCoords1 = accident.latLng;
            }

            let curSet = [];
            curSet.push(accident.id);

            window.dtpList.forEach(function (accident2) {
                if (accident.id == accident2.id) return;
                //Обходим все точки на предмет подсчёта расстояний
                if (accident2.distance!=undefined&&accident2.distance>moveBorder) {
                    setCoords2 = accident2.latLng_y;
                } else {
                    setCoords2 = accident2.latLng;
                }

                //Теперь сравниваем координаты между собой и если расстояние меньше порога - добавляем в сет
                let point1 = L.latLng(setCoords1.lat, setCoords1.lng);
                let point2 = L.latLng(setCoords2.lat, setCoords2.lng);

                distance = point1.distanceTo(point2);

                if (distance<=radius) curSet.push(accident2.id);
            });

            if (curSet.length>=maxAccidents) {
                dtpSets.push(curSet);
                curSet.forEach(function(accident){
                    if (jQuery.inArray(accident,accToShow)== -1)
                        accToShow.push(accident);
                });
            }
        });

        //$('#dangerAreas').text(dtpSets.length);
        clearMap();
        showPoints(accToShow);
        //console.log(dtpSets);
    });

    $('#clearAnalysedPoints').on('click',function(){
        $('#dangerAreas').text('');
        clearMap();
    });
});