<?php 
function json_fix_cyr($json_str) {
    $cyr_chars = array (
        '\u0430' => 'а', '\u0410' => 'А',
        '\u0431' => 'б', '\u0411' => 'Б',
        '\u0432' => 'в', '\u0412' => 'В',
        '\u0433' => 'г', '\u0413' => 'Г',
        '\u0434' => 'д', '\u0414' => 'Д',
        '\u0435' => 'е', '\u0415' => 'Е',
        '\u0451' => 'ё', '\u0401' => 'Ё',
        '\u0436' => 'ж', '\u0416' => 'Ж',
        '\u0437' => 'з', '\u0417' => 'З',
        '\u0438' => 'и', '\u0418' => 'И',
        '\u0439' => 'й', '\u0419' => 'Й',
        '\u043a' => 'к', '\u041a' => 'К',
        '\u043b' => 'л', '\u041b' => 'Л',
        '\u043c' => 'м', '\u041c' => 'М',
        '\u043d' => 'н', '\u041d' => 'Н',
        '\u043e' => 'о', '\u041e' => 'О',
        '\u043f' => 'п', '\u041f' => 'П',
        '\u0440' => 'р', '\u0420' => 'Р',
        '\u0441' => 'с', '\u0421' => 'С',
        '\u0442' => 'т', '\u0422' => 'Т',
        '\u0443' => 'у', '\u0423' => 'У',
        '\u0444' => 'ф', '\u0424' => 'Ф',
        '\u0445' => 'х', '\u0425' => 'Х',
        '\u0446' => 'ц', '\u0426' => 'Ц',
        '\u0447' => 'ч', '\u0427' => 'Ч',
        '\u0448' => 'ш', '\u0428' => 'Ш',
        '\u0449' => 'щ', '\u0429' => 'Щ',
        '\u044a' => 'ъ', '\u042a' => 'Ъ',
        '\u044b' => 'ы', '\u042b' => 'Ы',
        '\u044c' => 'ь', '\u042c' => 'Ь',
        '\u044d' => 'э', '\u042d' => 'Э',
        '\u044e' => 'ю', '\u042e' => 'Ю',
        '\u044f' => 'я', '\u042f' => 'Я',
 
        '\r' => '',
        '\n' => '<br />',
        '\t' => ''
    );
 
    foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
        $json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
    }
    return $json_str;
}
//Пробуем получить координаты по адресу
require_once('php-yandex-geo/autoload.php');

$api = new \Yandex\Geo\Api();

$fileJson = file_get_contents("dtp_2015_2016_2017.json");
$arTabs = json_decode($fileJson,true);

error_reporting(E_ERROR);

/*echo count($arTabs);

echo "<pre>";
print_r($arTabs);
echo "</pre>";

die();*/

$i = 1;
$sumCnt = count($arTabs);

$processCount = 6000;
foreach ($arTabs as &$tab) {
	if ($tab["latLng_y"]!=""&&is_array($tab["latLng_y"]) && count($tab["latLng_y"])==2)	 {
		//Уже обработано. Пропускаем
		$i++;
		echo "(".$i." from ".$sumCnt." - ".round($i*100./$sumCnt,2)."%) Tab ".$tab['id'].":    skipped \n";
		if ($i >= $processCount) break;
		else	continue;
	}
	echo "(".$i." from ".$sumCnt." - ".round($i*100./$sumCnt,2)."%) Tab ".$tab['id'].":    ";
	$api->setQuery($tab["addr"]);	
	// Настройка фильтров
	$api
		->setLimit(1) // кол-во результатов
		->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
		->load();

	$response = $api->getResponse();
	$collection = $response->getList();
	if (count($collection)>0) {
		$item = $collection[0];
		echo "found ".$item->getAddress().' ';
		$tab["latLng_y"] = ['lat'=>$item->getLatitude(),'lng'=>$item->getLongitude()];
		$tab["addr_y"] = $item->getAddress();
		
		//Сохраняем в файл
		$myfile = file_put_contents(/*$_SERVER["DOCUMENT_ROOT"]."/acc-analysis/*/"dtp_2015_2016_2017.json", json_fix_cyr(json_encode($arTabs)),  LOCK_EX);
		echo " saved\n";
	}
	if ($i >= $processCount) break;
	$i++;
}


/*// Или можно икать по адресу

echo "found: ".$response->getFoundCount()."<br/>"; // кол-во найденных адресов
echo "req: ".$response->getQuery()."<br/>"; // исходный запрос
echo "lat: ".$response->getLatitude()."<br/>"; // широта для исходного запроса
echo "long: ".$response->getLongitude()."<br/>"; // долгота для исходного запроса

$collection = $response->getList();

foreach ($collection as $item) {
    echo "addr: ".$item->getAddress()."<br>"; // вернет адрес
    echo "lat: ".$item->getLatitude()."<br>"; // широта
    echo "lng: ".$item->getLongitude()."<br>"; // долгота
	echo "<pre>";
    print_r($item->getData()); // необработанные данные
	echo "</pre>";
}*/
